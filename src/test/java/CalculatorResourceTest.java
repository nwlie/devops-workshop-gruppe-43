import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = "100 + 300 + 100";
        assertEquals(500, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = " 300 - 99 ";
        assertEquals(201, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = "100 x 10";
        assertEquals(1000, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = "100 / 10";
        assertEquals(10, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = "10 x 10 x 10";
        assertEquals(1000, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = "1000/10/10";
        assertEquals(10, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = "10*10";
        assertEquals(100, Integer.parseInt(calculatorResource.calculate(expression)));

        String throwTest = "100 y 10";
        assertTrue(calculatorResource.calculate(expression) instanceof String);

        expression = "100 + 100 + 100";
        assertEquals(300, Integer.parseInt(calculatorResource.calculate(expression)));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "1x8";
        assertEquals(8, calculatorResource.multiplication(expression));

        expression = "5x3";
        assertEquals(15, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "8/2";
        assertEquals(4,calculatorResource.division(expression));

        expression = "6/6";
        assertEquals(1,calculatorResource.division(expression));
    }
}
